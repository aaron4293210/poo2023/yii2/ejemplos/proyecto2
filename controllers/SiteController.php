<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionEjercicio1() {
        $alumnos = ['Mario', 'Ana', 'Maheva', 'Denis'];

        return $this->render('ejercicio1', [
            'alumnos' => $alumnos]);
    }

    public function actionEjercicio2a() {
        $fotos = ['a.jpg', 'b.jpg', 'c.jpg'];

        return $this->render('ejercicio2a', [
            'fotos' => $fotos]);
    }

    public function actionEjercicio2b() {
        $fotos = ['a.jpg', 'b.jpg', 'c.jpg'];

        return $this->render('ejercicio2b', [
            'fotos' => $fotos]);
    }

    public function actionEjercicio3(int $id = 0) {
        $alumnos = [
            [
                "nombre" => "Ana",
                "poblacion" => "Santoña",
                "imagen" => "a.jpg"
            ],
            [
                "nombre" => "Maheva",
                "poblacion" => "Santoña",
                "imagen" => "b.jpg",
            ],
            [
                "nombre" => "Alberto",
                "poblacion" => "Santoña",
                "imagen" => "c.jpg"
            ]
        ];

        $alumnoSinFoto = $alumnos[$id];
        unset($alumnoSinFoto['imagen']);

        return $this->render('ejercicio3', [
            'alumno' => $alumnos[$id],
            'alumnoSinFoto' => $alumnoSinFoto
        ]);
    }

    public function actionEjercicio4() {
        $numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];

        return $this->render('ejercicio4', [
            'numeros' => $numeros]);
    }
    
    public function actionEjercicio5() {
        $datos = [
            [
                "id" => 1,
                "nombre" => "Jorge",
                "poblacion" => "Santander",
                "direccion" => "Calle 1",
            ],
            [
                "id" => 2,
                "nombre" => "Ana",
                "poblacion" => "Santander",
                "direccion" => "Calle 2",
            ],
            [
                "id" => 3,
                "nombre" => "Maria",
                "poblacion" => "Santander",
                "direccion" => "Calle 3",
            ],
            [
                "id" => 4,
                "nombre" => "Carlos",
                "poblacion" => "Santander",
                "direccion" => "Calle 4",
            ]
        ];

        // Quiero que en la vista se muestre los datos anteriores utilizando CARDS
        // En el titulo de la CARD me coloca el id de cada elemento, en el texto de la card me coloca el nombre
        // Y el resto de los campos en una lista denetro de la CARD con el formato NomreCampo: Valor

        return $this->render('ejercicio5', [
            'datos' => $datos]);
    }

    public function actionEjercicio6() {
        $datos = [
            [
                "id" => 1,
                "nombre" => "Jorge",
                "poblacion" => "Santander",
                "direccion" => "Calle 1",
            ],
            [
                "id" => 2,
                "nombre" => "Ana",
                "poblacion" => "Santander",
                "direccion" => "Calle 2",
            ],
            [
                "id" => 3,
                "nombre" => "Maria",
                "poblacion" => "Santander",
                "direccion" => "Calle 3",
            ],
            [
                "id" => 4,
                "nombre" => "Carlos",
                "poblacion" => "Santander",
                "direccion" => "Calle 4",
            ]
        ];

        // Quiero que en la vista se muestre los datos anteriores utilizando CARDS
        // En el titulo de la CARD me coloca el id de cada elemento, en el texto de la card me coloca el nombre
        // Y el resto de los campos en una lista denetro de la CARD con el formato NomreCampo: Valor
        // Utilizando subvistas

        return $this->render('ejercicio6', [
            'datos' => $datos]);
    }
}
