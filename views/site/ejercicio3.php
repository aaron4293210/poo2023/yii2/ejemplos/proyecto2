<?php
    use yii\helpers\Html;

    /** @var yii\web\View $this */

    $this->title = 'Ejercicio3';
?>

<div class="row">
    <h1 class="text-center">Ejercicio3</h1>

    <div class="row mt-3">
        <div class="card col-md-6 mb-4 mx-auto">
            <div class="card-header">
                <?= Html::img(
                    "@web/imgs/{$alumno['imagen']}", [
                    'alt' => $alumno['imagen'], 
                    'class' => 'card-img-top col-sm-6 mx-auto',
                    ]) 
                ?>
            </div>
            <?= Html::ul($alumno, [
                'class' => 'list-group list-group-flush text-center', 
                // 'itemOptions' => 
                //     ['class' => 'list-group-item list-group-item-action']
                "item" => function ($datoAlumno, $indice) {
                        $salida = "";
                        
                        if ($indice != "imagen") {
                            $salida .= "<li class='list-group-item list-group-item-action'> {$datoAlumno} </li>";
                        }

                        return $salida;
                    }
                ]) 
            ?>
        </div>
    </div>
</div>