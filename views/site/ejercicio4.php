<?php
    use yii\helpers\Html;

    /** @var yii\web\View $this */

    $this->title = 'Ejercicio4';
?>

<div class="row">
    <h1>Ejercicio4</h1>

    <h5 class="col-12 text-center my-4">Con Foreach y Vistas</h5>

    <div class="row">
        <div class="col-12 d-flex justify-content-center">
            <?php
                echo $this->render('_listado', [
                    'numeros' => $numeros
                ]);
            ?>
        </div>
    </div>
</div>

<div class="row">
    <h5 class="col-12 text-center my-4">Con el helper HTML y sin vistas</h5>

    <div class="row">
        <div class="col-12 d-flex justify-content-center">
            <?= 
                Html::ul($numeros, [
                'class' => 'list-group list-group-horizontal', 
                'itemOptions' => 
                    ['class' => 'list-group-item list-group-item-action']
                ]) 
            ?>
        </div>
    </div>