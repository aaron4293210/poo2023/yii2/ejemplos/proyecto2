<?php
    use yii\helpers\Html;

    /** @var yii\web\View $this */

    $this->title = 'Ejercicio2a';
?>

<div class="row">
    <h1>Ejercicio2 - FOREACH CON VISTA</h1>

    <div class="row">
        <?php foreach ($fotos as $foto) { ?>
            <div class="card-group col-md-4 mb-4">
                <div class="card">
                    <div class="card-header">
                        <?= Html::img(
                            "@web/imgs/{$foto}", [
                            "alt' => '{$foto}'", 
                            'class' => 'card-img-top col-sm-6',
                            ]) 
                        ?>
                    </div>
                    <div class="card-body text-center">
                        <h4 class="card-title font-italic text-primary">FOTO</h4>
                        <p class="card-text lead text-muted"><?= $foto ?></p>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>