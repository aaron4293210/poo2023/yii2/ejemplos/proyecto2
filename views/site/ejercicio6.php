<?php
    use yii\helpers\Html;

    /** @var yii\web\View $this */

    $this->title = 'Ejercicio6';
?>

<div class="row">
    <h1>Ejercicio6</h1>
    
    <div class="row my-3">
        <?php foreach ($datos as $dato) { ?>
            <div class="col-sm-4 my-3 mb-sm-0">
                <div class="card text-center">
                    <div class="card-header">
                        Alumnos
                    </div>

                    <div class="card-body">
                        <h4 class="card-title font-italic text-primary"><?= $dato['id'] ?></h4>
                        <p class="card-text lead text-muted"><?= $dato['nombre'] ?></p>
                        <?=
                            $this->render('_ejercicio6', [
                                'dato' => $dato
                            ]);
                        ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>