<?php
    use yii\helpers\Html;

    /** @var yii\web\View $this */

    $this->title = 'Ejercicio1';
?>

<div class="row">
    <h1>Ejercicio1</h1>
    
    <h5 class="text-center my-3">Con foreach</h5>

    <div class="row">
        <div class="col-12 d-flex justify-content-center">
            <ul class="list-group text-center w-25">
                <?php foreach ($alumnos as $alumno) { ?>
                    <li class="list-group-item"><?= $alumno ?></li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <h5 class="text-center my-3">Con el helper HTML de YII2</h5>

    <div class="row">
        <div class="col-12 d-flex justify-content-center">
            <?= Html::ul($alumnos, [
                'class' => 'list-group text-center w-25', 
                'itemOptions' => 
                    ['class' => 'list-group-item']
                ]) 
            ?>
        </div>
    </div>
</div>