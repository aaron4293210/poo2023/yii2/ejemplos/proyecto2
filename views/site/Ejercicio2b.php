<?php
    use yii\helpers\Html;

    /** @var yii\web\View $this */

    $this->title = 'Ejercicio2b';
?>

<div class="row">
    <h1>Ejercicio2 - UL</h1>

    <div class="row">
        <?php
            echo Html::ul($fotos, [
                'class' => 'list-group list-group-horizontal',
                'item' => function ($foto) {
                    return 
                        Html::tag('li', 
                            Html::img("@web/imgs/{$foto}", [
                                "alt" => $foto,
                                "class" => "img-thumbnail col-md-3",
                                "style" => "width: 400px; height: 400px;",
                            ]),
                        ['class' => 'list-group-item']);
                }
            ]);
        ?>
    </div>
</div>

