<?php
    use yii\helpers\Html;
?>

<?= Html::ul($dato, [
    'class' => 'list-group list-group-flush', 
    "item" => function ($dato2, $indice) {
        if ($indice == "poblacion" || $indice == "direccion") {
            return "<li class='list-group-item list-group-item-action'>{$indice}: {$dato2} </li>";
        }
    }
]) ?>
            